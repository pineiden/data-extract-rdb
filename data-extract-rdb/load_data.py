from data_geo import GeoJSONData


def load_stations(server_name, datadb, log_path='~/log'):
    print("Obteniendo estaciones....", server_name, datadb)
    dbmanager = SessionCollector(log_path=log_path, active='true', server=server_name, **datadb)
    u = dbmanager.get_station_data(server=server_name)
    for m in u:
        # print(m)
        keys = ['id', 'code', 'db', 'dblist', 'ECEF_X', 'ECEF_Y', 'protocol_host',
                'ECEF_Z', 'port', 'protocol', 'host', 'dbname']
        try:
            station = dict(
                id=m['id'],
                code=m['st_code'],
                name=m['st_name'],
                ECEF_X=m['ecef_x'],
                ECEF_Y=m['ecef_y'],
                ECEF_Z=m['ecef_z'],
                db=m['db_code'],
                dblist=m['db_list'],
                port=m['st_port'],
                protocol=m['prt_name'],
                protocol_host=m['protocol_host'],
                host=m['st_host'],
                on_db=True
            )
            (ids, sta) = self.add_station(**station)
            # print(station)
        except Exception as exc:
            raise exc    
    dbmanager.close()
    return u

