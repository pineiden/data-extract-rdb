from orm_collector.manager import SessionCollector, object_as_dict
from rich import print
from networktools.geo import ecef2llh

def load_stations(server_name, datadb, log_path='~/log'):
    print("Obteniendo estaciones....", server_name, datadb)
    dbmanager = SessionCollector(
        log_path=log_path, 
        active='true', 
        server=server_name, 
        **datadb)
    u = dbmanager.get_station_data(server=server_name)
    print(u)
    dbmanager.close()
    return u


bridge="ssh -f -N4 -L 2345:localhost:5432 geodesia@10.54.217.15"

server_name = "atlas"
host = 'localhost'
port = 2345
dbdata =  dict(                                                                                                        
    dbuser="collector",                                                                           
    dbpass="XdataX",                                                                           
    dbname="collector",                                                                           
    dbhost=host,                                                                           
    dbport=port)

def get_stations():        
    u = load_stations(server_name, dbdata)
    stations = []

    for m in u:
        # print(m)
        keys = ['id', 'code', 'db', 'dblist', 'ECEF_X', 'ECEF_Y', 'protocol_host',
                'ECEF_Z', 'port', 'protocol', 'host', 'dbname']
        try:
            station = dict(
                id=m['id'],
                code=m['st_code'],
                name=m['st_name'],
                ECEF_X=m['ecef_x'],
                ECEF_Y=m['ecef_y'],
                ECEF_Z=m['ecef_z'],
                db=m['db_code'],
                dblist=m['db_list'],
                port=m['st_port'],
                protocol=m['prt_name'],
                protocol_host=m['protocol_host'],
                host=m['st_host'],
                on_db=True
            )
            x = float(station['ECEF_X'])
            y = float(station['ECEF_Y'])
            z = float(station['ECEF_Z'])
            (lat, lon, h) = ecef2llh(x, y, z)
            station["lat"] = lat
            station["lon"] = lon
            station["h"] = h

            stations.append(station)
            # print(station)
        except Exception as exc:
            raise exc

    print(stations)
    return stations


get_stations()
