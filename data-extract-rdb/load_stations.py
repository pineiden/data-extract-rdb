import csv
from pprint import pprint
from pathlib import Path

def read_station_list(path):
    data = []
    FLOAT_FIELDS = ["ECEF_X","ECEF_Y","ECEF_Z"]
    if path.exists():
        with open(path) as f:
            reader =  csv.DictReader(
                f, delimiter=';', quoting=csv.QUOTE_MINIMAL)
            for elem in reader:
                for field in FLOAT_FIELDS:
                    print("Converting")
                    elem[field] = float(elem.get(field))                  
                data.append(elem)
    return data


if __name__ == "__main__":
    path = Path("./station.csv")
    read_station_list(path)

