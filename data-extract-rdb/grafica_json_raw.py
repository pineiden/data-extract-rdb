import matplotlib.pyplot as plt
import numpy as np
#from my_toolbox.load_save import load_columns
from io import BytesIO
import json
import datetime
from pathlib import Path
from datetime import timedelta, datetime
import matplotlib.dates as mdates

import sys

def plot_traces(t, de, dn, du, t_origin=0.0, time_win=None,
                colors=('r', 'b', 'k'), color_origin='m',
                title=None, figsize=(18, 8),
                limits={}):
    # Three subplots sharing both x/y axes
    fig, (ax1, ax2, ax3) = plt.subplots(
      3, sharex=True, figsize=figsize)
    axes = (ax1, ax2, ax3)

    if title is not None:
      fig.suptitle(title, fontsize=22)
    ax3.set_xlabel(r'$t\, utc$', fontsize=16)
    ax1.set_ylabel(r'$X\,[m]$', fontsize=16)
    ax2.set_ylabel(r'$Y\,[m]$', fontsize=16)
    ax3.set_ylabel(r'$Z\,[m]$', fontsize=16)

    ax1.xaxis.set_major_formatter(mdates.DateFormatter("%m/%d %H:%M %Z"))
    ax2.xaxis.set_major_formatter(mdates.DateFormatter("%m/%d %H:%M %Z"))
    ax3.xaxis.set_major_formatter(mdates.DateFormatter("%m/%d %H:%M %Z"))

    
    result = [axe.set_xlim(**limits) for axe in axes]
    print(min(t), max(t))
    ax1.plot(t, de, color=colors[0])
    ax2.plot(t, dn, color=colors[1])
    ax3.plot(t, du, color=colors[2])
    return fig, axes


def jump_index(values, threshold):
    x=values
    #for k in range(x.size):
    #    if abs(x[k+1] - x[k]) > threshold:
    #        return k+1

from datetime import datetime

def conversor(elem):
    elem["DT_GEN"] = datetime.fromisoformat(elem.get("DT_GEN"))
    fields = ["X","Y","Z"]
    for field in fields:
        if field in elem:
            elem["data"][field]["value"]= elem["data"][field]["value"] if abs(elem["data"][field]["error"])<1 else np.nan
    return elem

def read_files(base_path, station, protocol='GSOF'):
    path=Path(f"{base_path}/{station}_{protocol}")
    all_data = []
    for elem in path.glob("*.json"):
        with open(elem) as f:
            data = json.load(f)
            dataset = data.get("dataset")
            all_data += dataset
    all_data = sorted(map(conversor, all_data), key=lambda e: e['DT_GEN'])
    return all_data




if __name__=="__main__":
    now = str(datetime.utcnow()).replace(" ","_")
    rtx_plot = Path(f"./RTX_{now}")
    rtx_plot.mkdir(parents=True)
    base_path = sys.argv[1]
    date_0 = datetime.fromtimestamp(int(sys.argv[2]))
    date_1 = datetime.fromtimestamp(int(sys.argv[3]))
    limits = {"left":date_0, "right":date_1}
    print(f"Dates {date_0} to {date_1}")
    stations = [elem.name.replace("_GSOF","") for elem in Path(base_path).glob("*_GSOF")]
    print(f"Ruta base {base_path}, stations {stations}")
    data={}
    for station in stations:
        dt=[]
        X=[]
        Y=[]
        Z=[]
        all_data = read_files(base_path, station)
        for row in all_data:
            try:
                if "ECEF" in row.keys():
                    dt.append(row["DT_GEN"])
                    X.append(row['ECEF']["X_POS"])
                    Y.append(row['ECEF']["Y_POS"])
                    Z.append(row['ECEF']["Z_POS"])
                else:
                    print("NO ECEF", station, row)
            except Exception as e:
                print(station, "---->", row)
                raise e
            

        data[station]={'dt':dt,'X':X,'Y':Y,'Z':Z}

        ts=data[station]['dt']

        X = np.array(data[station]['X'])
        Y = np.array(data[station]['Y'])
        Z = np.array(data[station]['Z'])

        if ts:

            ts_ref = ts[0]
            fig, (ax1, ax2, ax3) = plot_traces(
                ts, X, Y, Z,
                t_origin=ts_ref,
                time_win=None,
                colors=('r', 'b', 'k'),
                color_origin='m',
                title="%s" % station,
                limits=limits )
            fig.autofmt_xdate()
            fig_path = rtx_plot / station
            plt.savefig(str(fig_path))
            #plt.show()
            plt.close()
        del data[station]
