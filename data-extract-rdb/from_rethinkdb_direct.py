# std
import pickle
import json
import os
import asyncio
import argparse
from datetime import date, datetime, timedelta
from pathlib import Path
from multiprocessing import Pool
from networktools.colorprint import gprint, bprint, rprint

#
from pytz import timezone
# contrib
from networktools.colorprint import gprint, bprint, rprint
from networktools.environment import get_env_variable
from data_rdb import Rethink_DBS
import time


def read_args():
    parser = argparse.ArgumentParser(
        description="Obtener parámetros de operación")
    parser.add_argument('--inicio',
                        help="Fecha en timestamp inical",
                        type=float)
    parser.add_argument('--final', help="Fecha final", type=float)
    parser.add_argument('--dias', type=int, help="Cantidad de días")
    parser.add_argument('--path', type=Path, help="Ruta almacenamiento")
    parser.add_argument('--serializer', type=str, help="serializador")
    parser.add_argument('--nproc', type=int, help="Número de procesos")
    parser.add_argument('--compress', type=bool, help="Comprimir resultado")
    parser.add_argument('--host', type=str, help="Hostname or ip")
    parser.add_argument('--port', type=int, help="Database's port")
    parser.add_argument('--datatype', type=str, help="options: {RAW, ENU}")
    parser.add_argument('--dbname',
                        type=str,
                        help="ooptions: enu_data, collector")
    parser.add_argument("-g","--group", nargs='+',default=[], help="Grupo de estaciones")
    return parser






def tramos(inicio, final, horas=3):
    lista = []
    tiempo = inicio
    while tiempo <= final:
        lista.append(tiempo)
        tiempo += timedelta(hours=horas)
    lista.append(tiempo)
    return lista


async def session_rdb(code,
                      host='10.54.218.66',
                      port=28045,
                      dbname='enu_data'):
    kwargs = {
        "code": code,
        'host': host,
        'port': port,
        'dbname': dbname,
        "log_path":"/srv/data/gnss/log"
    }
    rdb = Rethink_DBS(**kwargs)
    result = await asyncio.wait_for(rdb.async_connect(), timeout=60)
    await rdb.select_db(kwargs.get('dbname'))
    await rdb.list_dbs()
    tables = await rdb.list_tables()
    return rdb, tables


class Data:
    def __init__(self, table, inicio, final, dataset):
        self.station, self.protocol = table.split("_")
        self.inicio = inicio
        self.final = final
        self.dataset = dataset

    @property
    def json(self):
        return dict(station=self.station,
                    protocol=self.protocol,
                    inicio=self.inicio.isoformat(),
                    final=self.final.isoformat(),
                    dataset=self.dataset)


def default(o):
    if isinstance(o, (date, datetime)):
        return o.isoformat()


async def save_table(
        lista_tramos,
        table, host,
        port,
        datatype,
        dbname,
        query_path,
        table_report=None):
    rdb, tablas = await session_rdb(table, host=host, port=port, dbname=dbname)
    # crear carpeta para tabla
    key = "DT_GEN"
    filter_opt = {'left_bound': 'open', 'index': key}
    path_table = query_path / table
    if not path_table.exists():
        path_table.mkdir(parents=True)
    for i, fecha in enumerate(lista_tramos[1::]):
        antes = lista_tramos[i]
        delta = [antes, fecha]
        file_name = "%s_a_%s.data" % (antes, fecha)
        file_name = file_name.replace(' ', "_")
        file_path = path_table / file_name
        if table_report:
            info = [
                antes, fecha, table,
                host, port, dbname,
                path_table, file_name
            ]
            table_report.add_row(*info)
        action = "wb"
        if serializer == json:
            file_name = "%s_a_%s.json" % (antes, fecha)
            file_name = file_name.replace(' ', "_")
            file_path = path_table / file_name
            action = "w"
        cursor = await asyncio.wait_for(rdb.get_data_filter(
            table, delta, filter_opt, key),
                                       timeout=200)
        lista = list(cursor)
        if lista:
            with open(file_path, action) as file_obj:
                data = Data(table, antes, fecha, lista)
                try:
                    if serializer == pickle:
                        serializer.dump(data, file_obj,
                                        pickle.HIGHEST_PROTOCOL)
                    else:
                        serializer.dump(data.json,
                                        file_obj,
                                        indent=2,
                                        default=default)
                except Exception as e:
                    rprint("Error jsoN")
                    print("Error al pasar a JSON", data.json, file_obj,
                          "Error", e)
                    raise e
    await rdb.close()


def data_save_table(
        lista_tramos,
        host,
        port,
        datatype,
        dbname,
        query_path,
        tabla,
        table_report=None):
    # match tabla con stations
    try:
        result = asyncio.run(
            save_table(
                lista_tramos,
                tabla,
                host,
                port,
                datatype,
                dbname,
                query_path,
                table_report))
    except Exception as e:
        print("No anda loop", e)
        raise e
    print("resultado", result)


from functools import partial


import re

async def query_data(inicio,
                     final,
                     query_path,
                     nproc,
                     compress=True,
                     datatype="ENU",
                     host='10.54.218.66',
                     port=28045,
                     dbname='enu_data',
                     serializer=pickle,
                     stations=[],
                     table_report=None):
    pool = Pool(processes=nproc)
    lista_tramos = tramos(inicio, final)
    rdb, tablas = await session_rdb("GRUPAL",
                                    host=host,
                                    port=port,
                                    dbname=dbname)
    if "ALL" in stations:
        stations =  tablas
    # join tablas con stations
    seleccion = list(
        filter(
            lambda tabla: any(
                       map(lambda station:tabla.startswith(station), stations)
                       ), tablas)
              )
    #
    data_save = partial(
        data_save_table,
        lista_tramos,
        host,
        port,
        datatype,
        dbname,
        query_path,
        table_report)
    r = pool.map_async(data_save, seleccion)
    r.wait()


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    parser = read_args()
    args = parser.parse_args()
    inicio_naive = datetime.fromtimestamp(args.inicio)
    tz = timezone("UTC")
    inicio = tz.localize(inicio_naive)
    final_naive = None
    if args.dias:
        final_naive = inicio + timedelta(args.dias)
    else:
        final_naive = datetime.fromtimestamp(args.final)
    serializer = pickle
    if args.serializer == "json":
        serializer = json
    final = tz.localize(final_naive)
    query_path=Path('./query')
    if args.path:
        query_path = Path(args.path)
    else:        
        query_path = Path("./query")
    query_path.mkdir(parents=True, exist_ok=True)
    nproc = 6
    if args.nproc:
        nproc = args.nproc
    host = '10.54.218.66'
    if args.host:
        host = args.host
    port = 28045
    if args.port:
        port = args.port
    datatype = "ENU"
    if args.datatype:
        datatype = args.datatype
    dbname = "enu_data"
    if args.dbname:
        dbname = args.dbname
    stations = []
    if args.group:
        stations = args.group
    asyncio.run(
        query_data(inicio,
                   final,
                   query_path,
                   nproc,
                   datatype=datatype,
                   host=host,
                   port=port,
                   dbname=dbname,
                   serializer=serializer,
                   stations=stations))
