import matplotlib.pyplot as plt
import numpy as np
#from my_toolbox.load_save import load_columns
from io import BytesIO
import json
import datetime
from pathlib import Path
from datetime import timedelta
import matplotlib.dates as mdates

import sys

def plot_traces(t, de, dn, du, t_origin=0.0, time_win=None,
                colors=('r', 'b', 'k'), color_origin='m',
                title=None, figsize=(18, 8)):
    # Three subplots sharing both x/y axes
    fig, (ax1, ax2, ax3) = plt.subplots(
      3, sharex=True, figsize=figsize)
    if title is not None:
      fig.suptitle(title, fontsize=22)
    ax3.set_xlabel(r'$t\, utc$', fontsize=16)
    ax1.set_ylabel(r'$E\,[m]$', fontsize=16)
    ax2.set_ylabel(r'$N\,[m]$', fontsize=16)
    ax3.set_ylabel(r'$U\,[m]$', fontsize=16)

    ax1.xaxis.set_major_formatter(mdates.DateFormatter("%H:%M"))
    ax2.xaxis.set_major_formatter(mdates.DateFormatter("%H:%M"))
    ax3.xaxis.set_major_formatter(mdates.DateFormatter("%H:%M"))

    ax1.plot(t, de, color=colors[0])
    ax2.plot(t, dn, color=colors[1])
    ax3.plot(t, du,  color=colors[2])
    return fig, (ax1, ax2, ax3)


def jump_index(values, threshold):
    x=values
    #for k in range(x.size):
    #    if abs(x[k+1] - x[k]) > threshold:
    #        return k+1

from datetime import datetime

def conversor(elem):
    elem["DT_GEN"] = datetime.fromisoformat(elem.get("DT_GEN"))
    fields = ["N","E","U"]
    for field in fields:
        if field in elem:
            elem["data"][field]["value"]= elem["data"][field]["value"] if abs(elem["data"][field]["error"])<1 else np.nan
    return elem

def read_files(base_path, station, protocol='GSOF'):
    path=Path(f"{base_path}/{station}_{protocol}")
    all_data = []
    for elem in path.glob("*.json"):
        with open(elem) as f:
            data = json.load(f)
            dataset = data.get("dataset")
            all_data += dataset
    all_data = sorted(map(conversor, all_data), key=lambda e: e['DT_GEN'])
    return all_data




if __name__=="__main__":    
    base_path = sys.argv[1]
    stations = [elem.name.replace("_GSOF","")
                for elem in Path(base_path).glob("*_GSOF")]
    print(f"Ruta base {base_path}")
    data={}
    for station in stations:
        dt=[]
        N=[]
        E=[]
        U=[]
        all_data = read_files(base_path, station)
        for row in all_data:
            dt.append(row["DT_GEN"])
            N.append(row['data']["N"]["value"])
            E.append(row['data']["E"]["value"])
            U.append(row['data']["U"]["value"])

        data[station]={'dt':dt,'N':N,'E':E,'U':U}

        ts=data[station]['dt']

        E = np.array(data[station]['E'])
        N = np.array(data[station]['N'])
        U = np.array(data[station]['U'])

        if ts:

            ts_ref = ts[0]

            fig, (ax1, ax2, ax3) = plot_traces(
              ts, E, N, U, t_origin=ts_ref, time_win=None,
                colors=('r', 'b', 'k'), color_origin='m', title="%s" % station )
            fig.autofmt_xdate()

            plt.savefig(f"./RTX/{station}")
            #plt.show()
            plt.close()
        del data[station]
