from pathlib import Path
import click
import rich
import asyncio
import json
import csv
from rich import print
from datetime import datetime, timezone, timedelta
from ..from_rethinkdb_direct import query_data

@click.option('--inicio',
              help="Fecha en timestamp inical",
              type=str)
@click.option('--final',
              help="Fecha final",
              type=str)
@click.option('--dias',
              default=0,
              type=int,
              help="Cantidad de días")
@click.option("--datetime-format",
              default="%d/%m/%Y %H:%M:%S",
              help="Format for datetime")
@click.option('--path',
              type=Path,
              help="Ruta almacenamiento")
@click.option('--serializer',
              type=str,
              help="Formato de guardad {json, csv, seed}")
@click.option('--nproc',
              type=int,
              help="Número de procesos")
@click.option('--compress',
              type=bool,
              help="Comprimir resultado")
@click.option('--host',
              type=str,
              default='10.54.218.39',
              help="Hostname or ip")
@click.option('--port',
              default=28045,
              type=int,
              help="Database's port")
@click.option('--datatype',
              type=str,
              default='ENU',
              help="options: {RAW, ENU}")
@click.option('--dbname',
              type=str,
              help="options: enu_data, collector, amqp_collector")
@click.option("--group",
              default="",
              help="Grupo de estaciones")
@click.option("--file-query",
              default="",
              help="File that contains the query")
def command(inicio, final, dias, datetime_format, path,
            serializer, nproc, compress, host, port, datatype, dbname,
            group, file_query):
    # asyncio loop
    loop = asyncio.get_event_loop()
    console = rich.Console()
    table = rich.Table()
    # datetime limits
    inicio_naive = datetime.strptime(inicio, datetime_format)
    final_naive = datetime.strptime(final, datetime_format)
    tz = timezone("UTC")
    inicio = tz.localize(inicio_naive)
    final = tz.localize(final_naive)
    if dias > 0:
        final = inicio + timedelta(days=dias)
    elif dias<0:
        inicio = final + timedelta(days=dias)
    # path to save files.
    query_path=Path('./query')
    if path:
        query_path = Path(path)
    else:        
        query_path = Path("./query")
    query_path.mkdir(parents=True, exist_ok=True)
    stations = "ALL"
    if group:
        stations = group.split()
    query_set = []
    if Path(file_query).exists():
        with open(file_query) as f:
            if file_query.endswith(".json"):
                data = json.load(f)
                query_set.append(data)
            elif file_query.endswith("*.csv"):
                reader = csv.DictReader(f)
                for elem in reader:
                    query_set.append(elem)

    fields = [
        "antes", "fecha", "table",
        "host", "port", "dbname",
        "path_table", "file_name"
    ]

    for field in fields:
        table.add_column(field)


    if query_set:
        for query in query_set:
            query["table_report"] = table
            asyncio.run(
                query_data(**query))
    else:
        query = dict(
            inicio=inicio,
            final=final,
            query_path=query_path,
            nproc=nproc,
            datatype=datatype,
            host=host,
            port=port,
            dbname=dbname,
            serializer=serializer,
            stations=stations,
            table_report=table
        )
        asyncio.run(
            query_data(**query))
    console.print(table)
