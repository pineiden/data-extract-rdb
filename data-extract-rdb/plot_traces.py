import matplotlib.pyplot as plt
import numpy as np
#from my_toolbox.load_save import load_columns

from io import BytesIO

import csv
import datetime

from datetime import timedelta
import matplotlib.dates as mdates



def plot_traces(t, de, dn, du, t_origin=0.0, time_win=None,
                colors=('r', 'b', 'k'), color_origin='m',
                title=None, figsize=(18, 8)):
    # Three subplots sharing both x/y axes
    fig, (ax1, ax2, ax3) = plt.subplots(
      3, sharex=True, figsize=figsize)
    if title is not None:
      fig.suptitle(title, fontsize=22)
    ax3.set_xlabel(r'$t\, utc$', fontsize=16)
    ax1.set_ylabel(r'$E\,[m]$', fontsize=16)
    ax2.set_ylabel(r'$N\,[m]$', fontsize=16)
    ax3.set_ylabel(r'$U\,[m]$', fontsize=16)

    ax1.xaxis.set_major_formatter(mdates.DateFormatter("%H:%M"))
    ax2.xaxis.set_major_formatter(mdates.DateFormatter("%H:%M"))
    ax3.xaxis.set_major_formatter(mdates.DateFormatter("%H:%M"))

    ax1.plot(t, de, color=colors[0])
    ax2.plot(t, dn, color=colors[1])
    ax3.plot(t, du,  color=colors[2])
    return fig, (ax1, ax2, ax3)


def jump_index(values, threshold):
    x=values
    #for k in range(x.size):
    #    if abs(x[k+1] - x[k]) > threshold:
    #        return k+1


stations = ('VALN_v2', 'TRPD_v2')

data={}

for station in stations:
    timestamp=[]
    N=[]
    E=[]
    U=[]

    file_name = station + '.csv'
    with open(file_name) as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            timestamp.append(float(row['timestamp']))
            N.append(row['N'])
            E.append(row['E'])
            U.append(row['U'])

    data[station]={'ts':timestamp,'N':N,'E':E,'U':U}
    
d=0
h=24
m=20
s=0
td=datetime.timedelta(days=d,hours=h, minutes=m,seconds=s)
d2=0
h2=23
m2=0
s2=0
tf=datetime.timedelta(days=d2,hours=h2, minutes=m2,seconds=s2)
now=datetime.datetime.utcnow()
snow=int((now-tf).strftime("%s"))
ts_ref = int((now-td).strftime("%s"))

im=plt.imread('logo.png')

def watermark_with(img, mark):
    img.seek(0)
    tmp = BytesIO()
    with Image.open(img) as base, Image.open(mark) as overlay:
        # It seems an alpha blend of .3 makes sense.
        marked = Image.blend(base, overlay, 0.3)
    marked.save(tmp, "PNG")
    return tmp


nmin=10
tsec=nmin*60

for st in stations:
    timestamp=data[st]['ts']

    ts=[datetime.datetime.utcfromtimestamp(ts) for (i,ts) in enumerate(timestamp) ]
    
    E = np.array(data[st]['E'])
    N = np.array(data[st]['N'])
    U = np.array(data[st]['U'])

    k_ev = jump_index(E, 0.02)

    fig, (ax1, ax2, ax3) = plot_traces(
      ts, E, N, U, t_origin=ts_ref, time_win=None,
        colors=('r', 'b', 'k'), color_origin='m', title="ST_%s" % st )
    fig.autofmt_xdate()

    plt.savefig(st)
    plt.show()
