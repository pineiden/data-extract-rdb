"""
Converta data from ECEF to ENU
Read the data from files, set a cursor, iterate over cursor
Needs the data AND the metadada (base position)
"""
from data_geo import GeoJSONData
from pathlib import Path
import ujson as json
from datetime import datetime
import numpy as np
from rich import print
#result = process_data.manage_data(c)


def conversor(elem):
    elem["DT_GEN"] = datetime.fromisoformat(elem.get("DT_GEN"))
    fields = ["X","Y","Z"]
    for field in fields:
        if field in elem:
            elem["data"][field]["value"] = (elem["data"][field]["value"] 
                                            if abs(elem["data"][field]["error"])<1 else np.nan)
    return elem

class ConvertECEF2ENU:
    """
    Set the metadata information, path for source and path for destiny
    """
    log_path = "./log"

    def __init__(self, protocol, metadata, ecef_path, enu_path):
        self.protocol = protocol
        self.process_data = self.add_process_instance()
        self.code = metadata.get("code")
        self.station = self.metadata.get("name")
        self.position  = self.metadata.get("position")
        self.source = ecef_path 
        self.destiny = enu_path 

    def convert(self):
        """
        Read all sources and convert to enu
        """
        path =  Path(f"{self.source}/{self.code}_{self.protocol}")
        all_data = []
        for elem in path.glob("*.json"):
            with open(elem) as f:
                data = json.load(f)
                dataset = data.get("dataset")
                all_data += dataset
        all_data = sorted(map(conversor, all_data), key=lambda e: e['DT_GEN'])
        enu_data = [self.process_data.manage_data(c) for c in
                    all_data]
        self.enu_data = enu_data
        return enu_data

    def plot(self):
        pass

    def save_enu(self):
        pass

    def add_process_instance(self):
        kwargs = {
            "code": self.code,
            "station": self.title,
            "position": self.position,
            "log_path": self.log_path / "geo_json_data"
        }
        process_instance = GeoJSONData(**kwargs)
        return process_instance

    @property
    def meta(self):
        return  {
            "code": self.code,
            "station": self.station,
            "position": self.position,
            "source": self.source,
            "destiny": self.destiny
        }
