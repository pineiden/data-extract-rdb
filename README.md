Extracción de datos desde Rethinkdb
============================================

Pre-instalación
----------------

Instalar los módulos listados en /git_install.txt/

Es importante el siguiente módulo.

```
datadbs-rethinkdb
```


Instalación
------------

Dentro del directorion en que se ubica *setup.py*

En modo desarrollo

```bash
pip install -e .
```

En modo producción

```bash
pip install .
```


Comando
--------

El comando necesita información que especificar, puede ser algo así:

```
python ./data-extract-rdb/from_rethinkdb.py --inicio $inicio --final $final --serializer "json" --nproc $NPROC --host $server --port $port --datatype "RAW" --dbname "collector"

```

En general, si está instalado debe tener activado el comando.

```
rdb_extract --help
```

Que mostrará las opciones.

Se pueden ingresar una a una como también mediante un json con la consulta o un
csv con un conjunto de consultas que tengan los campos necesarios.



Graficar
--------

Dispone de los script para graficar los dataset descargados


