from setuptools import setup

print("AVISO: Primero instalar ORM_COLLECTOR")

from pathlib import Path

path = Path(__file__).resolve().parent
with open(path/'README.md', encoding='utf-8') as f:
    long_description = f.read()

with open(path/'VERSION') as version_file:
    version = version_file.read().strip()

"""
TODO: comandos para plot PPP, GSOF
"""
setup(name='data_extract_rdb',
      version=version,
      description='Data exrtaction form rethinkdb database',
      url='http://gitlab.csn.uchile.cl/dpineda/data-extract-rdb',
      author='David Pineda Osorio',
      author_email='dpineda@csn.uchile.cl',
      license='MIT',
      packages=['data-extract-rdb', "tests", "scripts"],
      keywords = ["rethinkdb","gnss", "async", "multiprocess"],
      install_requires=[
          "networktools",
          "rethinkdb",
          "matplotlib",
          "numpy",
          "rich",
          "click"],
      entry_points={
        'console_scripts':[
            "rdb_extract = data_extract_rdb.commands.extract_data:command",]
        },
      include_package_data=True,
      long_description=long_description,
      long_description_content_type='text/markdown',
      zip_safe=False)
