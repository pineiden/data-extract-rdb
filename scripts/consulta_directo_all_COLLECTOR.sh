source ambiente
workon data_csn
echo "En ambiente, procesando"

#'--path' :: Ruta donde se guardan los archivos
#'--serializer',  {json, pickle}


# Aquí:
# seleccionar la fecha de inicio y final
# esto es un ejemplo:
# los datos se deben ingresar al script
# python en TIMESTAMP

# get all enu data for a day
inicio=$(date -d "2021-05-10" "+%s")
final=$(date -d "2021-05-11" "+%s")
server=10.54.217.15
port=28015
GROUP="ALL"
echo "$inicio hasta $final"

MARCA=$(date +"%Y_%m_%d_%H:%M:%S")
# guardando a json
python ./data-extract-rdb/from_rethinkdb_direct.py --inicio $inicio  --final $final --serializer "json" --nproc $NPROC --host $server --port $port --datatype "ENU" --dbname "collector" --group  $GROUP --path "/srv/data/gnss/query_${MARCA}"
# guardando a bytes, objetos python, archivos bin
# para python3.8
# python from_rethinkdb.py --inicio $inicio --final $final 

