source ambiente
workon data_csn
echo "En ambiente, procesando"

#'--path' :: Ruta donde se guardan los archivos
#'--serializer',  {json, pickle}


# Aquí:
# seleccionar la fecha de inicio y final
# esto es un ejemplo:
# los datos se deben ingresar al script
# python en TIMESTAMP

inicio=$(date -d "2021-01-17" "+%s")
final=$(date -d "2021-01-22" "+%s")
server=10.54.218.39
port=28015
GROUP="CONS PTRO FUTF RIO2 PB19"
echo "$inicio hasta $final"

MARCA=$(date +"%Y_%m_%d_%H:%M:%S")
DBNAME=amqp_collector
# guardando a json
python ./data-extract-rdb/from_rethinkdb_direct.py --inicio $inicio  \
--final $final \
--serializer "json" \
--nproc $NPROC \
--host $server \
--port $port \
--datatype "RAW" \
--dbname $DBNAME \
--group  $GROUP \
--path "/srv/data/gnss/$(echo $GROUP|sed 's/ /_/g')_query_${MARCA}"
# guardando a bytes, objetos python, archivos bin
# para python3.8
# python from_rethinkdb.py --inicio $inicio --final $final 

