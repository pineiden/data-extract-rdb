source ambiente
workon data_csn
echo "En ambiente, procesando"

#'--path' :: Ruta donde se guardan los archivos
#'--serializer',  {json, pickle}


# Aquí:
# seleccionar la fecha de inicio y final
# esto es un ejemplo:
# los datos se deben ingresar al script
# python en TIMESTAMP

inicio=$(date -d "2020-07-01" "+%s")
final=$(date -d "2020-07-31" "+%s")
server=10.54.217.15
port=28015

echo "$inicio hasta $final"

# guardando a json
python ./data-extract-rdb/from_rethinkdb.py --inicio $inicio --final $final --serializer "json" --nproc $NPROC --host $server --port $port --datatype "RAW" --dbname "collector"
# guardando a bytes, objetos python, archivos bin
# para python3.8
# python from_rethinkdb.py --inicio $inicio --final $final 
