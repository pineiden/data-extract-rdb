source ambiente
workon data_csn
echo "En ambiente, procesando"

#'--path' :: Ruta donde se guardan los archivos
#'--serializer',  {json, pickle}


# Aquí:
# seleccionar la fecha de inicio y final
# esto es un ejemplo:
# los datos se deben ingresar al script
# python en TIMESTAMP

inicio=$(date -d "-2 days" "+%s")
final=$(date -d "-1 day" "+%s")
server=10.54.218.15
port=29015

echo "$inicio hasta $final"

# guardando a json
python ./data-extract-rdb/from_rethinkdb.py --inicio $inicio --final $final --serializer "json" --nproc $NPROC --host $server --port $port --datatype "RAW"
# guardando a bytes, objetos python, archivos bin
# para python3.8
# python from_rethinkdb.py --inicio $inicio --final $final 
